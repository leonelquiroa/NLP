%% Load Data 
clear all;
% Load the example data.
filename = "weatherReports.csv";
data = readtable(filename,'TextType','string');
% Extract the text data from the field event_narrative, and the label data from the field event_type.
textData = data.event_narrative;
labels = data.event_type;
%% Preparation 
% Prepare String Data for Tokenizing
cleanTextData = erasePunctuation(textData);
% Convert the text data to lowercase.
cleanTextData = lower(cleanTextData);
% Create Tokenized Documents
cleanDocuments = tokenizedDocument(cleanTextData);
% Remove words with 2 or fewer characters, and words with 15 or greater characters.
cleanDocuments = removeShortWords(cleanDocuments,2);
cleanDocuments = removeLongWords(cleanDocuments,15);
% Normalize the words using the Porter stemmer.
% stemming is the process of reducing inflected (or sometimes derived) words 
% to their word stem, base or root form�generally a written word form
cleanDocuments = normalizeWords(cleanDocuments);
% remove stop words such as "a", "the", and "in"
cleanDocuments = removeWords(cleanDocuments,stopWords);
% Create a bag-of-words model.
cleanBag = bagOfWords(cleanDocuments);
% Remove words that do not appear more than two times in the bag-of-words model.
cleanBag = removeInfrequentWords(cleanBag,2);
% Remove empty documents from the bag-of-words model and the corresponding labels from labels.
[cleanBag,idx] = removeEmptyDocuments(cleanBag);
labels(idx) = [];
%% Compare with Raw Data
rawDocuments = tokenizedDocument(textData);
rawBag = bagOfWords(rawDocuments);
% Calculate the reduction in data.
numWordsClean = cleanBag.NumWords;
numWordsRaw = rawBag.NumWords;
reduction = 1 - numWordsClean/numWordsRaw;
% Compare the raw data and the cleaned data by visualizing the two bag-of-words models using word clouds.
figure
subplot(1,2,1)
wordcloud(rawBag);
title("Raw Data")
subplot(1,2,2)
wordcloud(cleanBag);
title("Clean Data")

%% Create a Preprocessing Function
newText = "A tree is downed outside Apple Hill Drive, Natick";
newDocuments = preprocessWeatherNarratives(newText);

function stemData = preprocessWeatherNarratives(newText)
    NoPuncData = erasePunctuation(newText);
    LowerData = lower(NoPuncData);
    tokenData = tokenizedDocument(LowerData);
    NoStopData = removeWords(tokenData,stopWords);
    rangeData = removeShortWords(NoStopData,2);
    rangeData = removeLongWords(rangeData,15);
    stemData = normalizeWords(rangeData);
end