clear all;
% import the answers text data
txt = extractFileText("Intuit_HomePages_Web-UX.txt");
% split all the answers into lines
answers = split(txt,newline);
% erase the punctuation
answers = erasePunctuation(answers);
% tokenize the data
documents = tokenizedDocument(answers);
% convert to lowercase
documents = lower(documents);
% remove stop words such as "a", "the", and "in"
docsNoStop = removeWords(documents,stopWords);
% Create a bag-of-words model
bag = bagOfWords(docsNoStop);
% show the cloud words
figure; wordcloud(bag);