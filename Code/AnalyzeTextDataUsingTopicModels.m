% Load and Extract Text Data
clear all;
data = readtable("weatherReports.csv",'TextType','string');
% Extract the text data from the field event_narrative.
textData = data.event_narrative;
% prepare the text data
documents = preprocessWeatherNarratives(textData);
% Fit LDA Model
% Latent Dirichlet allocation (LDA) is a generative statistical model 
% that allows sets of observations to be explained
% by unobserved groups that explain why some parts of the data are similar
bag = bagOfWords(documents);
% Remove words from the bag-of-words model that have do not appear more than 2 times in total. 
bag = removeInfrequentWords(bag,2);
% Remove any documents containing no words from the bag-of-words mode
bag = removeEmptyDocuments(bag);
% Fit an LDA model with 60 topics
numTopics = 60;
mdl = fitlda(bag,numTopics);
% Visualize Topics Using Word Clouds
figure;
for topicIdx = 1:4
    subplot(2,2,topicIdx)
    wordcloud(mdl,topicIdx);
    title("Topic: " + topicIdx)
end
% View Mixtures of Topics in Documents
newDocument = tokenizedDocument("A tree is downed outside Apple Hill Drive, Natick");
topicMixture = transform(mdl,newDocument);
figure
bar(topicMixture)
xlabel("Topic Index")
ylabel("Probabilitiy")
title("Document Topic Probabilities")
% Visualize multiple topic mixtures using stacked bar charts. 
figure
topicMixtures = transform(mdl,documents(1:10));
barh(topicMixtures(1:10,:),'stacked')
xlim([0 1])
title("Topic Mixtures")
xlabel("Topic Probability")
ylabel("Document")
%% 
function [documents] = preprocessWeatherNarratives(textData)
    % Erase punctuation.
    cleanTextData = erasePunctuation(textData);
    % Convert the text data to lowercase.
    cleanTextData = lower(cleanTextData);
    % Tokenize the text.
    documents = tokenizedDocument(cleanTextData);
    % Remove a list of stop words.
    documents = removeWords(documents,stopWords);
    % Remove words with 2 or fewer characters, and words with 15 or greater
    % characters.
    documents = removeShortWords(documents,2);
    documents = removeLongWords(documents,15);
    % Normalize the words using the Porter stemmer.
    documents = normalizeWords(documents);
end