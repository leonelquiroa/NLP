clear all;
% import the answers text data
txt = extractFileText("Intuit_HomePages_Web-UX.txt");
% split all the answers into lines
answers = split(txt,newline);
% erase the punctuation
answers = erasePunctuation(answers);
% tokenize the data
documents = tokenizedDocument(answers);
% convert to lowercase
documents = lower(documents);
% remove stop words such as "a", "the", and "in"
docsNoStop = removeWords(documents,stopWords);
% show the top-20 more repetitive words
k = 20;
% no repetitive
bag1 = bagOfWords(documents);
T1 = topkwords(bag1,k);
disp(T1);
% all words
bag2 = bagOfWords(docsNoStop);
T2 = topkwords(bag2,k);
disp(T2);