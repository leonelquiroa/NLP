clear all;
% import the answers text data
txt = extractFileText("Intuit_HomePages_Web-UX.txt");
% split all the answers into lines
answers = split(txt,newline);
% Clean String Array
TF = (answers == "");
answers(TF) = [];
% Replace some punctuation marks with space characters
p = [".","?","!",",",";",":"];
answers = replace(answers,p," ");
% Strip leading and trailing space characters from each element
answers = strip(answers);
% remove stop words such as "a", "the", and "in"
% answers = removeWords(answers,stopWords);
% Split answers into a string array whose elements are individual words
allWords = strings(0);
for i = 1:length(answers)
   allWords = [allWords ; split(answers(i))];
end

%% Sort Words Based on Frequency
% Find the unique words in sonnetWords. Count them and sort them based on their frequency.
allWords = lower(allWords);
[words,~,idx] = unique(allWords);
numOccurrences = histcounts(idx,numel(words));
% Sort the words in sonnetWords by number of occurrences, from most to least common.
[rankOfOccurrences,rankIndex] = sort(numOccurrences,'descend');
wordsByFrequency = words(rankIndex);
% Display the twenty most common words
disp(wordsByFrequency(1:20));

%% Plot Word Frequency
loglog(rankOfOccurrences);
xlabel('Rank of word (most to least common)');
ylabel('Number of Occurrences');

%% Collect Basic Statistics in Table
numOccurrences = numOccurrences(rankIndex);
numOccurrences = numOccurrences';
numWords = length(allWords);
T = table;
T.Words = wordsByFrequency;
T.NumOccurrences = numOccurrences;
T.PercentOfText = numOccurrences / numWords * 100.0;
T.CumulativePercentOfText = cumsum(numOccurrences) / numWords * 100.0;
disp(T(1:10,:));