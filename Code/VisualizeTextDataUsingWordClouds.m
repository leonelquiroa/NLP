% Load the example data
clear all;
filename = "weatherReports.csv";
T = readtable(filename,'TextType','string');
% Extract the text data from the event_narrative column.
textData = T.event_narrative;
% Create a word cloud from all the weather reports.
figure
wordcloud(textData);
title("Weather Reports")
% Compare the words in the reports with labels "Hail" and "Thunderstorm Wind". 
% Create word clouds of the reports for each of these labels. 
% Specify the word colors to be blue and magenta for each word cloud respectively.
figure
labels = T.event_type;

subplot(1,2,1)
idx = labels == "Hail";
wordcloud(textData(idx),'Color','blue');
title("Hail")

subplot(1,2,2)
idx = labels == "Thunderstorm Wind";
wordcloud(textData(idx),'Color','magenta');
title("Thunderstorm Wind")

% Compare the words in the reports from the states Florida, Kansas, and Alaska. 
% Create word clouds of the reports for each of these states in rectangles and draw a border around each word cloud.

figure
state = T.state;

subplot(1,3,1)
idx = state == "FLORIDA";
wordcloud(textData(idx),'Shape','rectangle','Box','on');
title("Florida")

subplot(1,3,2)
idx = state == "KANSAS";
wordcloud(textData(idx),'Shape','rectangle','Box','on');
title("Kansas")

subplot(1,3,3)
idx = state == "ALASKA";
wordcloud(textData(idx),'Shape','rectangle','Box','on');
title("Alaska")

% Compare the words in the reports with property damage reported in thousands of dollars to the reports with damage reported in millions of dollars

cost = T.damage_property;
idx = endsWith(cost,"K");
figure
wordcloud(textData(idx),'HighlightColor','blue');
title("Damage Reported in Thousands")

idx = endsWith(cost,"M");
figure
wordcloud(textData(idx),'HighlightColor','red');
title("Damage Reported in Millions")